package com.example.easynotes.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.easynotes.exception.ResourceNotFoundException;
import com.example.easynotes.models.Note;
import com.example.easynotes.repositories.NoteRepository;



@RestController
@RequestMapping("/queryapi")
public class NoteQueryController {

	@Autowired
	NoteRepository noteRepository;
	
// Melihat semua data Notes
	@GetMapping("/notes/all")
	public List<Note> findAllData() {
		   return noteRepository.findAll();
	}
	
	
//	 Untuk menghapus notes
	 @DeleteMapping("/notes/{id}")
	 public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long noteId) {
	     Note note = noteRepository.findById(noteId)
	             .orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));

	     noteRepository.delete(note);

	     return ResponseEntity.ok().build();
	 }	
	
//	 untuk meng-insert
	@PostMapping("/notes")
	public HashMap<String, Object> insertDataNote(@Valid @RequestBody Note note) {	
				
		HashMap<String, Object> hash = new HashMap<String, Object>();
		
		int total = noteRepository.insertNote(note.getContent(), note.getTitle());
		
		hash.put("Message :", "Input Success");
		hash.put("Data :", total);
		
		return hash;
	}
	
//	Untuk melihat 1 data
	 @GetMapping("/notes/{id}")
	 public Note getNoteById(@PathVariable(value = "id") Long noteId) {
	     return noteRepository.findById(noteId)
	    		 .orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));
	}
	 
//	Untuk update data
	@PutMapping("/notes/{id}")
	public HashMap<String, Object> updateNote(@PathVariable(value = "id") Long noteId,
	                                         @Valid @RequestBody Note noteDetails) {

		HashMap<String, Object> hash = new HashMap<String, Object>();
	
	   Note note = noteRepository.findById(noteId)
	           .orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));
	   
	   if (note.getTitle() == null) {
		   note.setTitle(noteDetails.getTitle());
	   }
	   if (note.getContent() == null) {
		   note.setContent(noteDetails.getContent());
	   }
	    
	    Note updatedNote = noteRepository.save(note);
	    
	    hash.put("Data" , updatedNote );
	    
	    return hash;
	 }
}
