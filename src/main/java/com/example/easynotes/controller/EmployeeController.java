package com.example.easynotes.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.easynotes.exception.ResourceNotFoundException;
import com.example.easynotes.models.Employee;
import com.example.easynotes.models.Note;
import com.example.easynotes.repositories.EmployeeReposiroty;

@RestController
@RequestMapping("/api")
public class EmployeeController {

	@Autowired
	EmployeeReposiroty employeeReposiroty;
	
//	Melihat semua data employee
	@GetMapping("/employees")
	public List<Employee> getAllEmployees() {
		return employeeReposiroty.findAll();
	}
	
//	Menambahkan seorang employee baru
	@PostMapping("/employees")
	public Employee createEmployee(@Valid @RequestBody Employee employee) {
		return employeeReposiroty.save(employee);
	}
	
	
//	Menambahkan banyak employee baru
	@PostMapping("/moreemployees")
	public HashMap<String, Object> createMoreEmployee(@Valid @RequestBody Employee ...employees ) {
			
		HashMap<String, Object> mapEmployee = new HashMap<String, Object>();
		ArrayList<Employee> listEmployee = new ArrayList<Employee>();
		
		for (Employee emp : employees) {
			employeeReposiroty.save(emp);
			listEmployee.add(emp);
		}
		
		mapEmployee.put("Message :" , "Create Success");
		mapEmployee.put("Total Insert : ", listEmployee.size());
		mapEmployee.put("Data insert :" , listEmployee);
		
		return mapEmployee;
	}
	
	
//	 Melihat satu data employee
	@GetMapping("/employees/id/{id}")
	public Employee getEmployeeById(@PathVariable(value = "id") Long employeeId) {
		 return employeeReposiroty.findById(employeeId)
		    .orElseThrow(() -> new ResourceNotFoundException("Note", "id", employeeId));
	}
				
//	Melihat employee jobdesc
	@GetMapping("/employees/job/{jobDescription}")
	public HashMap<String, Object> employeeJob(@PathVariable(value = "jobDescription") String jobDescription) {
		
		//	menyimpan list dari findd all
		List<Employee> list = getAllEmployees();
			
		//	membuat hashmap untuk menyimpan data
		HashMap<String, Object> el = new HashMap<String, Object>();
		
		//	Array list untuk menyimpan data	
		ArrayList<Employee> listEmployee = new ArrayList<Employee>(); 
		
		for (Employee l : list) {
			if (l.getJobDescription().equalsIgnoreCase(jobDescription))  {
				listEmployee.add(l);	
			}
		}
		
		el.put("Message", "All Programmer");
		el.put("Total", listEmployee.size());
		el.put("Data", listEmployee);
		
		return el;				
	}
	
//	method mencari highest salary
	public int higestSalary() {
		List<Employee> employees = getAllEmployees();
		
		int higestSalary = employees.get(0).getSalary();
		
		for (Employee emp : employees) {
			if (emp.getSalary() > higestSalary) {
				higestSalary = emp.getSalary();
			}
		}
		return higestSalary;
	}

	
//	mencari salary tertinggi
	@GetMapping("/employees/{highestsalary}")
	public HashMap<String, Object> ShowHigestSalary() {
			
		List<Employee> employees = getAllEmployees();
		HashMap<String, Object> hasEmployee = new HashMap<String, Object>();	
		ArrayList<Employee> listEmp = new ArrayList<Employee>();
		
		int temp = higestSalary();
		
		for (Employee emp : employees) {	
			
			if(emp.getSalary() == temp) {
				listEmp.add(emp);
			}
		}
		hasEmployee.put("Total Employee : ", listEmp.size());
		hasEmployee.put("Highest Salary : ", listEmp);
		
		return hasEmployee;
	}

	
//	Method untuk mencari umur total umur
	public int countAge() {
		
		List<Employee> employees = getAllEmployees();
		
		int allAge = 0;
		
		for (Employee ems : employees) {
			
			allAge = allAge + ems.getAge();		
		}
		
		return allAge;
	}
	
	
//	melihat employee yang memiliki umur diatas rata-rata
	@GetMapping("/employees/age/{aboveaverage}")
	public HashMap<String, Object> getEmployeeOldAge() {
		
		List<Employee> employees = getAllEmployees();
		
		//	mencari umur diatas rata-rata
		int empl = countAge()/employees.size();
		
		HashMap<String, Object> allData = new HashMap<String, Object>();
		
		ArrayList<Employee> listEmployee = new ArrayList<Employee>();
		
		for (Employee ems : employees) {
			
			if(ems.getAge() > empl) {
				listEmployee.add(ems);
			}
		}
		
		allData.put("Message : ", "Read Succes");
		allData.put("Rata-rata Umur : ", empl);
		allData.put("Data : ", listEmployee);
		
		return allData;
	}

//	method untuk mencari nama yang emngandung kata
	@GetMapping("/employees/filter/{name}")
	public HashMap<String, Object> getFilteringName(@PathVariable(value = "name") String name) {
		List<Employee> list = getAllEmployees();
		
		HashMap<String, Object> hashEmployee = new HashMap<String, Object>();
		ArrayList<Employee> employees = new ArrayList<Employee>();
		
		for (Employee emp: list) {
			
			if (emp.getName().toLowerCase().contains(name)) {
				employees.add(emp);
			}
		}
		
		hashEmployee.put("Data :" , employees);
		return hashEmployee;
	}
	
//	Update data enployee
	@PutMapping("/employees/update/{id}")
	public Employee updateEmployee(@PathVariable(value= "id") Long employeeId, @Valid @RequestBody Employee employeeDetails) {
		
		Employee employee = employeeReposiroty.findById(employeeId)
				.orElseThrow(() -> new ResourceNotFoundException("Note", "id", employeeId));
		
		employee.setName(employeeDetails.getName());
		employee.setAddress(employeeDetails.getAddress());
		employee.setAge(employeeDetails.getAge());
		employee.setJobDescription(employeeDetails.getJobDescription());	
		employee.setSalary(employeeDetails.getSalary());
	
		Employee update = employeeReposiroty.save(employee);
		
		return update;
	}
	
//	Menghapus data employee
	 @DeleteMapping("/employees/{id}")
	 public ResponseEntity<?> deleteEmployee(@PathVariable(value = "id") Long employeeId) {
	     Employee employee = employeeReposiroty.findById(employeeId)
	             .orElseThrow(() -> new ResourceNotFoundException("Note", "id", employeeId));

	     employeeReposiroty.delete(employee);

	     return ResponseEntity.ok().build();
	 }
	
	
	
}
