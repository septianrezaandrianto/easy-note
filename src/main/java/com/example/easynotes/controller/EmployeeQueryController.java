package com.example.easynotes.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.easynotes.exception.ResourceNotFoundException;
import com.example.easynotes.models.Employee;
import com.example.easynotes.repositories.EmployeeReposiroty;

@RestController
@RequestMapping("/queryapi")
public class EmployeeQueryController {

	@Autowired
	EmployeeReposiroty employeeReposiroty;
	
// 	Menampilkan All Employee
	@GetMapping("/employees/all")
		public List<Employee> findAllData() {
			return employeeReposiroty.findAll();
	}
	
//	Melihat satu data employee
	@GetMapping("/employees/id/{id}")
	public Employee getEmployeeById(@PathVariable(value = "id") Long employeeId) {
		 return employeeReposiroty.findById(employeeId)
		    .orElseThrow(() -> new ResourceNotFoundException("Note", "id", employeeId));
	}
		
//	Meng-insert data baru
	@PostMapping("/employees")
	public HashMap<String, Object> insertDataEmployee(@Valid @RequestBody Employee employee) {	
			
		HashMap<String, Object> hash = new HashMap<String, Object>();

		int total = employeeReposiroty.insertEmployee(employee.getAddress(), employee.getAge(), employee.getJobDescription(), employee.getName(), employee.getSalary());
			
		hash.put("Message :", "Input Success");
		hash.put("Data :", total);
		return hash;
	}
	
//	Meng-edit data baru
	@PutMapping("/employees/{id}")
	public HashMap<String, Object> updateDataEmployee(@PathVariable(value = "id") Long employeeId, @Valid @RequestBody Employee employeeDetails) {	
			
		HashMap<String, Object> hash = new HashMap<String, Object>();
		
		 Employee employee = employeeReposiroty.findById(employeeId)
		            .orElseThrow(() -> new ResourceNotFoundException("Note", "id", employeeId));
		   
		 	if(employee.getAddress() == null) {
		 		
		 		employee.setAddress(employeeDetails.getAddress());
		 	}
		 	if(employee.getAge() == 0) {
		 		employee.setAge(employeeDetails.getAge());
		 	}
		 	
		 	if(employee.getJobDescription() == null ) {
		 		employee.setJobDescription(employeeDetails.getJobDescription()); 		 	
		 	}
		 
		 	if(employee.getName() == null ) {
		 		employee.setName(employeeDetails.getName());
		 	}
		 	
		 	if(employee.getSalary() == 0) {	
		 		employee.setSalary(employeeDetails.getSalary());
		 	}
		 	
		 	
		 	Employee updatedEmployee = employeeReposiroty.save(employee);
		   
		 	hash.put("Data :", updatedEmployee);
		 	
		return hash;
	}
		
	
	
//	Menghapus data employee
	@DeleteMapping("/employees/{id}")
	public ResponseEntity<?> deleteEmployee(@PathVariable(value = "id") Long employeeId) {
		Employee employee = employeeReposiroty.findById(employeeId)
				.orElseThrow(() -> new ResourceNotFoundException("Note", "id", employeeId));

		employeeReposiroty.delete(employee);

		return ResponseEntity.ok().build();
	}
	
}
