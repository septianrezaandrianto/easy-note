package com.example.easynotes.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "writers")
@EntityListeners(AuditingEntityListener.class)
public class Writer {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long writerId;
	
	@Column(name = "firstName")
	@NotBlank
	private String firstName;
	
	@Column(name = "lastName")
	@NotBlank
	private String lastName;
	
	@Column(name = "age")
	private int age;

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Writer Id : " + writerId 
				+ "\nFirst Name : " + firstName 
				+ "\nLast Name : " + lastName 
				+ "\nAge : " + age;
	}
	
	
	
}
