package com.example.easynotes.repositories;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.easynotes.models.Employee;

@Repository
public interface EmployeeReposiroty extends JpaRepository<Employee, Long>{

//	Query untuk menampilkan semua data
	@Query
	(value= "SELECT * FROM employees AS e",nativeQuery = true)
	List<Employee> findAllData();
	
//	Query untuk menambah data / insert into	
	@Modifying
	@Query
	(value = "INSERT INTO employees (address, age, job_description, name, salary) VALUES (:address, :age, :job_description, :name, :salary)", nativeQuery = true)
	@Transactional
	int insertEmployee(@Param("address") String address, @Param("age") int age, @Param("job_description") String jobDescription, @Param("name") String name, @Param("salary") int salary);
	
//	Query untuk mendelete data
	@Modifying
	@Query
	(value="DELETE FROM employees WHERE employee_Id = ?1", nativeQuery = true)
	void deleteByUserId(Long employeeId);
	
//	Query untuk meng-update data
	@Modifying
	@Query
	(value = "UPDATE employees AS e SET e.address = :address, e.age = :age, e.job_description = :job_description, e.name = :name, e.salary = :salary WHERE e.employee_id = employeeId", nativeQuery = true)
	@Transactional
	int updateEmployee(@Param("address") String address, @Param("age") int age, @Param("job_description") String jobDescription, @Param("name") String name, @Param("salary") int salary);
	
}
