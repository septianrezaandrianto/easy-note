package com.example.easynotes.repositories;

import com.example.easynotes.models.Note;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;




@Repository
public interface NoteRepository extends JpaRepository<Note, Long> {

//	Query untuk menampilkan semua data
	@Query
	(value= "SELECT * FROM notes AS n",nativeQuery = true)
	List<Note> findAllData();
	
//	Query untuk mendelete data
	@Modifying
	@Query
	(value="DELETE FROM notes WHERE id = ?1", nativeQuery = true)
	void deleteByUserId(Long id);
	

//	Query untuk menambah data / insert into	 Belum Work
	@Modifying
	@Query
	(value = "INSERT INTO notes (content, created_at, title, updated_at) VALUES (:content , NOW(), :title, NOW())", nativeQuery = true)
	@Transactional
	int insertNote(@Param("content") String content, @Param("title") String title);
	
	
//	Query untuk meng-update data
	@Modifying
	@Query
	(value = "UPDATE notes AS n SET n.content = :content, n.created_at = :created_at, n.title = :title, n,updated_at = :NOW()", nativeQuery = true)
	@Transactional
	int updateEmployee(@Param("content") String content, @Param("title") String title);
	
	
	
}
